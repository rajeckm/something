<?php
session_start();
include "style.php";
include "conf.php";
$conn = mysqli_connect($host, $user, $pass, $db);
if ($conn->connect_error) {
    // Zijn de gegevens goed ingevoerd?
    die("Error");

}

?>

<body>
    <?php if(!$_SESSION['userLoggedIn'] == true && $_SESSION['adminLoggedIn'] == false):
    ?>
    <div class="notLoggedIn">
        <br>
            <p>Hi, je bent niet ingelogd. Om producten te kunnen bestellen, moet je ingelogd zijn. Je kan hieronder registeren of inloggen.</p>

            <li class="nav-item login">
                <a class="nav-link" href="#"><i>Register of login</i></a>
            </li>
    

    <?php elseif($_SESSION['adminLoggedIn'] == true):
    ?>
        <div class="notLoggedIn">
        <br>
            <p>Hi, je bent ingelogd als een administrator. Om producten te kunnen bestellen, moet je ingelogd zijn als een klant. Je kan hieronder uitloggen en daarna weer inloggen als klant</p>
            <a href="logout.php">Log uit</a>
    <?php endif; ?>
    



</body>