<?php
session_start();
include "style.php";
error_reporting(E_ALL);
ini_set('display_errors', 1);
$conn = mysqli_connect($host, $user, $pass, $db);
if ($conn->connect_error) {
    die("error");
}

$_SESSION["contactVerzonden"] = 0;


if (isset($_POST["verzend"])) {
    $name = mysqli_real_escape_string($conn, $_POST["name"]);
    $mail = mysqli_real_escape_string($conn, $_POST["mail"]);
    $comment = mysqli_real_escape_string($conn, $_POST["comment"]);

    if ($name == NULL && $comment == NULL ) {
        $_SESSION["nameAndMailFail"] = true;
    }
    elseif ($name == NULL) {
        $_SESSION["nameFail"] = true;
    }
    elseif ($comment == NULL) {
        $_SESSION["commentFail"] = true;
    }
    else {
        $query = "INSERT INTO contact (name, mail, comment) VALUES ('$name', '$mail', '$comment')";
        if ($conn->query($query)) {
            $_SESSION["contactVerzonden"] = true;
        } else {
            $_SESSION["contactFail"] = true;
        }

    }


}



?>


<html>
    <head>
        <title>Contact</title>
    </head>


    <body>
<div class="container">
        <form action="contact.php" method="post">
        <div class="form-group">
            <br><input type="text" name="name" placeholder="Naam"><br><br>
            <input type="text" name="mail" placeholder="E-mail"><br><br>
            <label for="comment">Comment:</label>
            <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
            <input type="submit" name="verzend" value="Verzend">
</form>
</div>

    <?php if ($_SESSION["contactVerzonden"] == true): ?>
    <br><div class="alert alert-success">
    <strong>Success!</strong> Je bericht is verzonden. Bedankt voor je input!
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php  endif; $_SESSION["contactVerzonden"] = false;?>

    <?php if ($_SESSION["contactFail"] == true): ?>
    <br><div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Error!</strong> Helaas is je bericht niet verzonden. Probeer het opnieuw. Blijft dit bericht terugkomen? Neem dan contact op met de webbeheerder.
    </div>
    <?php endif; $_SESSION["contactFail"] = false;?>

    <?php if ($_SESSION["nameAndMailFail"] == true): ?>
    <br><div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Error!</strong> Helaas is je bericht niet verzonden. Check of je naam én je bericht is ingevuld, dit is nu namelijk niet het geval.
    </div>
    <?php endif; $_SESSION["nameAndMailFail"] = false;?>

    <?php if ($_SESSION["nameFail"] == true): ?>
    <br><div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Error!</strong> Helaas is je bericht niet verzonden. Check of je naam goed is ingevuld, dit is nu namelijk niet het geval.
    </div>
    <?php endif; $_SESSION["nameFail"] = false;?>

    <?php if ($_SESSION["commentFail"] == true): ?>
    <br><div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Error!</strong> Helaas is je bericht niet verzonden. Check of je comment goed is ingevuld, dit is nu namelijk niet het geval.
    </div>
    <?php endif; $_SESSION["commentFail"] = false;?>



</div>

</body>