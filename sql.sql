-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 27 jan 2019 om 21:36
-- Serverversie: 5.6.35
-- PHP-versie: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `test1`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `administrators`
--

CREATE TABLE `administrators` (
  `name` varchar(25) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `administrators`
--

INSERT INTO `administrators` (`name`, `mail`, `password`) VALUES
('Administrator', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `contact`
--

CREATE TABLE `contact` (
  `name` varchar(25) NOT NULL,
  `mail` varchar(35) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `contact`
--

INSERT INTO `contact` (`name`, `mail`, `comment`) VALUES
('', '', ''),
('Karin', 'fam.massa@kpnmail.nl', 'Hallo dit is een test'),
('Rajeck', 'rajeckm@hotmail.com', 'Beste,\r\n\r\nDit werkt.\r\n\r\nMet vriendelijke groet,\r\nRajeck');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `mail` varchar(25) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`username`, `password`, `mail`, `rank`) VALUES
('abc', '900150983cd24fb0d6963f7d28e17f72', 'abc@abc.nl', 0),
('abcde', 'ab56b4d92b40713acc5af89985d4b786', 'abcde@abcde.nl', 0),
('abcdef', '5d793fc5b00a2348c3fb9ab59e5ca98a', 'abcdef@abcdef.com', 0),
('ajskfjasja', '0cc175b9c0f1b6a831c399e269772661', 'aha@a.com', 0),
('asdf', '8fa14cdd754f91cc6554c9e71929cce7', 'fam.massa@kpnmail.nl', 0),
('f', '0cbc6611f5540bd0809a388dc95a615b', 'f@f.com', 0),
('fuck', '79cfdd0e92b120faadd7eb253eb800d0', 'fuck@fuck.fuck', 0),
('Hai', '42810cb02db3bb2cbb428af0d8b0376e', 'hai@hai.nl', 0),
('Mwep', 'c0013fbf97896128ff8e444b1b79b64c', 'mwep@mwep.com', 0),
('Rajeckm', '96d33e7087b74ae41ca17e86970a0acf', 'rajeckm@hotmail.com', 1),
('TestAccount', '2c9341ca4cf3d87b9e4eb905d6a3ec45', 'test@test.nl', 0);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `administrators`
--
ALTER TABLE `administrators`
  ADD UNIQUE KEY `name` (`name`,`mail`);

--
-- Indexen voor tabel `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`name`,`mail`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `mail` (`mail`);
