<?php
session_start();
if (!$_SESSION['adminLoggedIn'] == true) {
    header('Location: http://localhost/');
}
include "style.php";
include "conf.php";
error_reporting(E_ALL);
ini_set('display_errors', 1);
$conn = mysqli_connect($host, $user, $pass, $db);
if ($conn->connect_error) {
    // Zijn de gegevens goed ingevoerd?
    die("Error");

}


function getNr() {
    include "conf.php";
    $conn = mysqli_connect($host, $user, $pass, $db);
    $allNr = "SELECT nr FROM products";
    $resultAllNr = mysqli_query($conn, $allNr);

    
    while (list($nr) = mysqli_fetch_row($resultAllNr)) {
        echo "<option value='$nr'>$nr</option>";
    }
}


if (isset($_POST["getProduct"])) {
    $getThisNR = $_POST["thisNr"];
    $getProductNameByNr = "SELECT name FROM products WHERE nr = '$getThisNR'";
    $getProductPriceByNr = "SELECT price FROM products WHERE nr = '$getThisNR'";
    $getProductSortByNr = "SELECT sort FROM products WHERE nr = '$getThisNR'";
    $getProductBrandByNr = "SELECT brand FROM products WHERE nr = '$getThisNR'";
    $getProductSpecsByNr = "SELECT specs FROM products WHERE nr = '$getThisNR'";
    $getProductPhotoByNr = "SELECT photo FROM products WHERE nr = '$getThisNR'";

    $resultGetProductNameByNr = mysqli_query($conn, $getProductNameByNr);
    $resultGetProductPriceByNr = mysqli_query($conn, $getProductPriceByNr);
    $resultGetProductSortByNr = mysqli_query($conn, $getProductSortByNr);
    $resultGetProductBrandByNr = mysqli_query($conn, $getProductBrandByNr);
    $resultGetProductSpecsByNr = mysqli_query($conn, $getProductSpecsByNr);
    $resultGetProductPhotoByNr = mysqli_query($conn, $getProductPhotoByNr);
    $_SESSION["productAddedToChange"] = true;

    $getThisNumber = $_SESSION["thisNumber"];
    $_SESSION["thisNumber"] = $getThisNR;
    $number = $_SESSION["thisNumber"];


}

if (isset($_POST["updateProduct"])) {
    $newName = $_POST["newName"];
    $newSort = $_POST["newSort"];
    $newBrand = $_POST["newBrand"];
    $newPrice = $_POST["newPrice"];
    $newPicture = $_POST["newPicture"];
    $newSpecs = $_POST["newSpecs"];

    $number = $_SESSION["thisNumber"];

    $updateProduct = "UPDATE products SET name='$newName', price='$newPrice', sort='$newSort', brand='$newBrand', specs='$newSpecs', photo='$newPicture', nr='$number' where nr='$number'";
    mysqli_query($conn, $updateProduct) or trigger_error(mysqli_error($conn));

}


if (ISSET($_POST["addProduct"])) {
    $productName = $_POST["productName"];
    $productPrice = $_POST["productPrice"];
    $productSort = $_POST["productSort"];
    $productBrand = $_POST["productBrand"];
    $productSpecs = $_POST["productSpecs"];
    $productPicture = $_POST["productPicture"];

    $addProduct = "INSERT INTO products (name, price, sort, brand, specs, photo) VALUES ('$productName', '$productPrice', '$productSort', '$productBrand', '$productSpecs', '$productPicture')";
    if ($conn->query($addProduct)) {
        $_SESSION["productAdded"] = true;
    }
    else {
        $_SESSION["productAddFail"] = true;
    }
}


?>

<html>
    <head>
        <title>Producten wijzigen</title>
    </head>



    <body>
        <br>
        <?php if($_SESSION["productAddedToChange"] == false): ?>
        <div class="container">
        <h1>Producten toevoegen</h1>
        <form action="adminproducten.php" method="post">
            <div class="form-group">
            Productnaam: <input type="text" name="productName"><br>
            Soort product: <input type="text" name="productSort"><br>
            Merk: <input type="text" name="productBrand"><br>
            Prijs: <input type="number" name="productPrice"><br>
            Link naar de foto: <input type="text" name="productPicture" placeholder="http://xx.xx"><br>
            <label for="comment">Specificaties:</label>
            <textarea class="form-control" rows="5" id="comment" name="productSpecs"></textarea>
            <input type="submit" name="addProduct" value="Voeg product toe">
        </form>
</div>
<?php endif; ?>

<br>
        <?php if($_SESSION["productAddedToChange"] == true): ?>
        <div class="container">
        <h1>Producten wijzigen</h1>
        <form action="adminproducten.php" method="post">
            <div class="form-group">
            Productnaam: <input type="text" name="newName"><br>
            Soort product: <input type="text" name="newSort"><br>
            Merk: <input type="text" name="newBrand"><br>
            Prijs: <input type="number" name="newPrice"><br>
            Link naar de foto: <input type="text" name="newPicture" placeholder="http://xx.xx"><br>
            <label for="comment">Specificaties:</label>
            <textarea class="form-control" rows="5" id="comment" name="newSpecs"></textarea>
            <input type="submit" name="updateProduct" value="Update het product">
        </form>
</div>
<?php endif; ?>


        <?php if ($_SESSION["productAddedToChange"] == false): ?>
        <br><h1> Producten wijzigen: </h1>
        <form action="adminproducten.php" method="post">
            NR: <select name="thisNr"> 
                <?php
                getNr();
                ?>
                </select>

            <input type="submit" name="getProduct">
        </form>
        <?php endif; ?>


        <?php if ($_SESSION["productAddedToChange"] == true): ?>
        <br><h1>Huidige gegevens:</h1>
        
            <b>Naam:</b> <?php  while (list($getProductName) = mysqli_fetch_row($resultGetProductNameByNr)) {
        echo $getProductName;
         }?><br>
            <b>Soort product:</b> <?php  while (list($getProductSort) = mysqli_fetch_row($resultGetProductSortByNr)) {
        echo $getProductSort;
         }?><br>
            <b>Merk:</b> <?php  while (list($getProductBrand) = mysqli_fetch_row($resultGetProductBrandByNr)) {
        echo $getProductBrand;
         }?><br>
            <b>Prijs:<b> <?php  while (list($getProductPrice) = mysqli_fetch_row($resultGetProductPriceByNr)) {
        echo $getProductPrice;
         }?><br>
            <b>Specs: <?php while(list($getProductSpecs) = mysqli_fetch_row($resultGetProductSpecsByNr)) {
        echo $getProductSpecs;
        }?><br>
            <b>Foto: <img src="<?php  while (list($getProductPhoto) = mysqli_fetch_row($resultGetProductPhotoByNr)) {
        echo $getProductPhoto;
         }?>" style="width:300px;height:400px;">




    <?php endif; $_SESSION["productAddedToChange"] = false;?>
        </div>


        

    <br><h1> Producten verwijderen: </h1>
        <form action="adminproducten.php" method="post">
            NR: <select name="thisMwep"> 
                <?php
                getNr();

                ?>
                </select>

            <input type="submit" name="getThisProduct">
        </form>
        <?php
        if (ISSET($_POST["getThisProduct"])) {
            $removeNr = $_POST["thisMwep"];
            $removeItem = "DELETE FROM products WHERE nr = '$removeNr'";
        }

        ?>
    </body>

</html>



    <?php if ($_SESSION["productAdded"] == true): ?>
    <br><div class="alert alert-success">
    <strong>Succes!</strong> Het product is toegevoegd.
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php  endif; $_SESSION["productAdded"] = false;?>

    <?php if ($_SESSION["productAddFail"] == true): ?>
    <br><div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Error!</strong> Helaas is het product niet aangepast. Probeer het opnieuw!
    </div>
    <?php endif; $_SESSION["productAddFail"] = false;?>
    



