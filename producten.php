<?php
session_start();
include "style.php";
error_reporting(E_ALL);
ini_set('display_errors', 1);

$conn = mysqli_connect($host, $user, $pass, $db);
if ($conn->connect_error) {
    die("error");
}

$getRows = mysqli_query($conn, "SELECT COUNT(*) as `count` FROM products");
$fetchRows = mysqli_fetch_assoc($getRows);
$count = $fetchRows['count'];

?>

<html>

<head>
    <title>Producten</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>



<body>

<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">Onze producten</h5>
        <div class="row">
            <!-- Producten -->
            <?php
            $nr = 0;


while ($nr < $count) {
    $nr++;
    $getName = "SELECT name FROM products WHERE nr = '$nr'";
    $getPrice = "SELECT price FROM products WHERE nr = '$nr'";
    $getBrand = "SELECT brand FROM products WHERE nr = '$nr'";
    $getSort = "SELECT sort FROM products WHERE nr = '$nr'";
    $getPhoto = "SELECT photo FROM products WHERE nr = '$nr'";
    $getSpecs = "SELECT specs FROM products WHERE nr = '$nr'";
    $resultGetName = mysqli_query($conn, $getName);
    $resultGetPrice = mysqli_query($conn, $getPrice);
    $resultGetBrand = mysqli_query($conn, $getBrand);
    $resultGetSort = mysqli_query($conn, $getSort);
    $resultGetPicture = mysqli_query($conn, $getPhoto);
    $resultGetSpecs = mysqli_query($conn, $getSpecs); ?>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="<?php while (list($pic) = mysqli_fetch_row($resultGetPicture)) {
                                        echo $pic;
                                    } ?>" alt="card image"></p>
                                    <h4 class="card-title"><?php
                    while (list($name) = mysqli_fetch_row($resultGetName)) {
                        echo $name; ?></h4>
                                    <p class="card-text"><?php  while (list($brand) = mysqli_fetch_row($resultGetBrand)) {
                            echo $brand; } ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title"><p><?php
                                    
                                        echo $name;
                        }
                     ?></h4>
                                    <p class="card-text"><?php
                                     while (list($sort) = mysqli_fetch_row($resultGetSort)) {
                                    echo $sort;
                                    }  
                                    echo "<br>";
                                    while(list($specs) = mysqli_fetch_row($resultGetSpecs)) {
                                        echo $specs;
                                    }?> </p>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <p>Prijs: &euro;<?php
                                                while (list($price) = mysqli_fetch_row($resultGetPrice)) {
                                                    echo $price;
                                                } ?><br>
                                                <form action="bestel.php" method="post">
                                                    <input type="hidden" name="id" value='<?php echo $nr; ?>'>
                                                    <button type="submit" name="send"><i class="fas fa-cart-plus fa-2x"></i></button>
                                             </form>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // Producten -->
            <?php
}
?>
          
            
</body>