<?php
include "conf.php";

?>

<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" href="/style.css">
<script type="text/javascript" src="/js.js"></script>
<title>PCHub</title>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top"> <!-- Begin NAV-bar -->
  <a class="navbar-brand" href="http://localhost/"><img src="https://img.icons8.com/cotton/2x/monitor.png" style="width:40px;height:40px;"></a>
  <!-- Linkjes in de NAV-bar -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="contact.php">Contact</a>
    </li>
    <?php if ($_SESSION["adminLoggedIn"] == true): ?>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Beheerdersdashboard
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="/admindashboard.php">Dashboard</a>
        <a class="dropdown-item" href="/admincontact.php">Contact</a>
        <a class="dropdown-item" href="/adminproducten.php">Producten wijzigen</a>
		<a class="dropdown-item" href="#">Klanten beheren</a>
      </div>
    </li>
    <?php elseif ($_SESSION["userLoggedIn"]): ?>
    <li class="nav-item">
        <a class="nav-link" href="/klantdashboard.php">Klantendashboard</a>
    </li>
    <?php endif; ?>

    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Producten
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="/producten.php">Alle producten</a>
        <a class="dropdown-item" href="#">Videokaarten</a>
        <a class="dropdown-item" href="#">Moederborden</a>
		<a class="dropdown-item" href="#">Processorkoelers</a>
		<a class="dropdown-item" href="#">Processors</a>
      </div>
    </li>
  </ul>
  
    <div class="navbar-collapse collapse w-10 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="winkelmand.php"><i class="fas fa-shopping-cart fa-2x "></i></a>
            </li>

            <?php if ($_SESSION["adminLoggedIn"] == true || $_SESSION["userLoggedIn"] == true): ?>
            <li class="nav-item">
            <a class="nav-link" href="logout.php"><i class="fas fa-sign-out-alt fa-2x"></i></a>
            </li>
        <?php else:  ?>
        <li class="nav-item login">
        <a class="nav-link" href="#"><i class="fas fa-sign-in-alt fa-2x"></i></a>
        </li>
        <?php endif ?>

   
        </ul>
</div>
</nav> <!-- Einde NAV-bar -->

<!-- Login- en registreer modal -->

    <div class="cd-user-modal"> 
        <div class="cd-user-modal-container"> 
            <ul class="cd-switcher">
                <li><a href="#0">Inloggen</a></li>
                <li><a href="#0">Registreren</a></li>
            </ul>


            <div id="cd-login"> <!-- Om in te loggen -->
            <p class="cd-form-message">Klanten log-in</p>
                <form action="login.php" method="post" class="cd-form" >
                    
                    <p class="fieldset">
                    
                        <label class="image-replace cd-email" for="signin-email">E-mail</label>
                        <input class="full-width has-padding has-border" id="signin-email" type="text" name="username" placeholder="Gebruikersnaam">
                        
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" >Wachtwoord</label>
                        <input class="full-width has-padding has-border" name="password" id="signin-password" type="text"  placeholder="Wachtwoord">
                        <a href="#0" class="hide-password">Verberg</a>
                        
                    </p>

                  

                    <p class="fieldset">
                        <input class="full-width" type="submit" name="login" value="Inloggen">
                    </p>
                </form>

                <p class="cd-form-bottom-message"><a href="#0">Ben je een administrator?</a></p>
            </div> 

            

            <div id="cd-signup"> <!-- Registreren -->
            <p class="cd-form-message">Registreren</p>

                <form action="register.php" method="post" class="cd-form" >
                    <p class="fieldset">
                        <label class="image-replace cd-username" for="signup-username">Gebruikersnaam</label>
                        <input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Gebruikersnaam" name="username">
                        
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-email" for="signup-email">E-mail</label>
                        <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail" name="mail">
                        
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signup-password">Wachtwoord</label>
                        <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Wachtwoord" name="password">
                        <a href="#0" class="hide-password">Verberg</a>
                        
                    </p>
					
					
                    <p class="fieldset">
                        <input type="checkbox" name="checkbox" id="accept-terms">
                        <label for="accept-terms">Ik ga akkoord met de <a href="http://localhost/terms.html">voorwaarden.</a></label>
                    </p>

                    <p class="fieldset">
                        <input class="full-width has-padding" type="submit" value="Registreren" name="verzend">
                    </p>
                </form>

                
               
            </div> 

            <div id="cd-admin-login"> <!-- Administrator log-in -->
                <p class="cd-form-message">Log als administrator hier in.</p>

                <form action="administratorlogin.php" method="post" class="cd-form" >
            

                    <p class="fieldset">
                        <label class="image-replace cd-email" for="signin-email">Name</label>
                        <input class="full-width has-padding has-border" id="signin-email" type="text" name="adminName" placeholder="Gebruikersnaam">
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" >Wachtwoord</label>
                        <input class="full-width has-padding has-border" name="adminPassword" id="signin-password" type="text"  placeholder="Wachtwoord">
                        <a href="#0" class="hide-password">Verberg</a>
                        
                    </p>

                    <p class="fieldset">
                        <input class="full-width has-padding" name="adminLogin" type="submit" value="Inloggen">
                    </p>
                </form>

                <p class="cd-form-bottom-message"><a href="#0">Terug naar gebruikers log-in</a></p>
            </div> 
            
            
        </div> 
    </div> <!-- Eind registreer en login-modal -->

        <!-- Elke error staat hieronder aangegeven -->
        <?php if ($_SESSION["loginError"] == true): ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error!</strong> Je gegevens zijn niet gevonden in de database. Heb je alles wel goed ingevuld?
        </div>
    <?php  endif; $_SESSION["loginError"] = false;?>

    <?php if ($_SESSION["noCheckbox"] == true): ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error!</strong> Je hebt onze <a href="http://localhost/terms.html">algemene voorwaarden</a> niet geaccepteerd. Deze moet je wel accepteren om te kunnen registeren!
        </div>
    <?php endif; $_SESSION["noCheckbox"] = false; ?>

    <?php if ($_SESSION["registerError"] == true): ?>
    <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error!</strong> De registratie is mislukt. Probeer het opnieuw of neem contact op met de webbeheerder.
        </div>
    <?php endif; $_SESSION["registerError"] = false;?>

    <?php if ($_SESSION["mailRegisterError"] == true): ?>
    <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error!</strong> Deze mail is helaas al in gebruik. Probeer een ander of log in!
        </div>
    <?php endif; $_SESSION["mailRegisterError"] = false; ?>

    <?php if ($_SESSION["usernameRegisterError"] == true): ?>
    <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error!</strong> Deze gebruikersnaam is helaas al in gebruik. Probeer een andere!
        </div>
    <?php endif; $_SESSION["usernameRegisterError"] = false;  ?>
</html>